#!/usr/bin/env python

import argparse
import os
from datetime import (
    datetime,
    timedelta,
    )
import textwrap

from hamster import client

HLINE = "-"*72
class TimeSheet(object):
    def __init__(self, project, from_date, end_date):
        self.project = project
        self.from_date = from_date
        self.end_date = end_date
        self.from_date_str = from_date.strftime("%Y-%m-%d")
        self.end_date_str = end_date.strftime("%Y-%m-%d")
        self.total_time = timedelta(minutes=0)
        self.fact_list = []

        # get the facts
        storage = client.Storage()
        raw_fact_list = storage.get_facts(from_date, end_date, project)

        # filter the fact-list to include only the 'given
        # project's facts and compute total time spent
        for fact in raw_fact_list:
            if fact.category == project:
                if fact.delta:
                    self.total_time += fact.delta
                self.fact_list.append(fact)


def long_timesheet(timesheet):
    print(HLINE)
    print("Developer : %s, Project: %s" % \
          (os.getlogin(), timesheet.project))
    print("From date : %s, To date: %s" % \
          (timesheet.from_date_str, timesheet.end_date_str))
    print("Total     : %s" % (timesheet.total_time,))
    print(HLINE)
    for fact in timesheet.fact_list:
        date_str = fact.start_time.strftime("%Y-%m-%d")
        from_time_str = fact.start_time.strftime("%H:%M")
        end_time_str = " WIP "
        if fact.end_time:
            end_time_str = fact.end_time.strftime("%H:%M")
        line = "%s %s to %s, %s\n" % \
               (date_str, from_time_str, end_time_str, fact.delta)
        print(line)
        wrap = textwrap.wrap(fact.activity, 70,
                             initial_indent='  ', subsequent_indent='  ')
        for line in wrap:
            print(line)
        if fact.description:
            desc = fact.description.splitlines()
            print('\n')
            for line in desc:
                wrap = textwrap.wrap(line, 70,
                                     initial_indent='  ',
                                     subsequent_indent='  ',
                                     replace_whitespace=False,
                                     drop_whitespace=False)
                for line in wrap:
                    print(line)
        print(HLINE)


def short_timesheet(timesheet):
    print(HLINE)
    print("Developer : %s, Project: %s" % \
          (os.getlogin(), timesheet.project))
    print("From date : %s, To date: %s" % \
          (timesheet.from_date_str, timesheet.end_date_str))
    print(HLINE)
    for fact in timesheet.fact_list:
        date_str = fact.start_time.strftime("%Y-%m-%d")
        from_time_str = fact.start_time.strftime("%H:%M")
        end_time_str = " WIP "
        if fact.end_time:
            end_time_str = fact.end_time.strftime("%H:%M")
        line = "%10s %5s-%5s | %8s | %s" % \
               (date_str, from_time_str, end_time_str,
                fact.delta, fact.activity)
        line = line[0:72]
        print(line)

    print(HLINE)
    print("%22s | %s" % ("Total", timesheet.total_time))
    print(HLINE)

def main():
    parser = argparse.ArgumentParser(
        description="Generate timesheet from hamster")
    parser.add_argument("project", metavar="PROJECT",
                        help="Project code")
    parser.add_argument("from_date", metavar="YYYY-MM-DD",
                        help="Start date")
    parser.add_argument("end_date", metavar="YYYY-MM-DD", nargs='?',
                        help="End date", default=None)
    parser.add_argument("--long", action='store_true', default=False,
                        help="Include long description")
    args = parser.parse_args()
    from_date = datetime.strptime(args.from_date, "%Y-%m-%d")
    end_date = datetime.today()
    if args.end_date:
        end_date = datetime.strptime(args.end_date, "%Y-%m-%d")
    timesheet = TimeSheet(args.project, from_date, end_date)

    if args.long:
        long_timesheet(timesheet)
    else:
        short_timesheet(timesheet)

if __name__ == "__main__":
    main()

# Local Variables:
# mode: python
# indent-tabs-mode: nil
# tab-width: 4
# End:
