
A tool to generate a time-sheet from hamster. No fancy shit. Just a
simple plain-text report for one hamster category.

Example: For a hamster category named reham,

    # ./reham reham 2013-11-01 2013-11-30
    ------------------------------------------------------------------------
    Developer : joe, Project: reham
    From date : 2013-11-01, To date: 2013-11-30
    ------------------------------------------------------------------------
    2013-11-06 00:12-11:57 | 11:45:00 | test activity
    2013-11-07 00:13-11:58 | 11:45:00 | initial reham work
    2013-11-08 00:13-11:58 | 11:45:00 | test activity
    2013-11-12 21:30-01:30 |  4:00:00 | this long string should be truncated
    2013-11-21 20:30-23:15 |  2:45:00 | test activity
    2013-11-29 23:07-23:08 |  0:01:00 | initial reham work
    2013-11-29 23:09-00:12 |  1:03:00 | initial reham work
    2013-11-30 00:12- WIP  |  0:14:52 | initial reham work
    ------------------------------------------------------------------------
                     Total | 1 day, 19:18:52
    ------------------------------------------------------------------------
